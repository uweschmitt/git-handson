# encoding: utf-8
from __future__ import print_function, division, absolute_import

def greet(name):
    return "hello %s" % name

def say_hello(name):
    print(greet(name))

def greet_two(name_1, name_2):
    return "hi %s and %s" % (name_1, name_2)
